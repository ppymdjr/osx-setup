#!/bin/bash

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# remove any existing emacs app
sudo rm -rf /Applications/Emacs.app

brew tap railwaycat/emacsmacport
brew cask install emacs-mac

# need to handle caps lock remapping. The easy bit is ->ctrl but I'd like to get the escape in there too, which requires installing and configuring those keyboard modifying programs

rm -rf .emacs.d emacs.d

cd 
git clone git@gitlab.com:ppymdjr/emacs.d.git
ln -s emacs.d .emacs.d

# open it to make it install the packagecase 
# this doesn't work due to OSX security - it must be opened manually
# open /Applications/Emacs.app

# I should get some common lisp stuff set up really...

# see here: https://www.saltycrane.com/blog/2017/07/how-map-caps-lock-escape-when-tapped-and-control-when-held-mac-os-sierra/
mkdir -p ~/.config/karabiner/
cp karabiner.json ~/.config/karabiner/
# this will popup a security warning and the program must be allowed in system preferences
# it also seems to query the keyboard type again. It works though :)
brew cask install karabiner-elements

# this looks useful too - I'll check it out...
# brew cask install hammerspoon

cp gpg-wrapper /usr/local/bin/

# !!! I need to add GPG, Kr etc for git signing


brew install git

cp dot-profile ~/.profile
